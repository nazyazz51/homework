let number = parseInt(prompt('Enter the number'));

while (isNaN(number) || number < 0 || !Number.isInteger(number)) {
    console.log('sorry, not found');
    number = parseInt(prompt('Enter the number'))
}

let count = (number / 5);

if (count > 0) {
    for (let i = 1; i <= count; i++){
        console.log(i * 5);
    }
} else {
    console.log('sorry');
}