/*
let firstName = prompt('Enter your name, please');
let lastName = prompt('Enter your surname');
let birthday = prompt('Введите дату рождения (в формате dd.mm.yyyy)');
let currentDate = new Date();



function createNewUser () {
    let newUser = {
        firstName,
        lastName,
        birthday,
        getAge() {
            birthday = birthday.split('.');
            birthday.forEach((item) => {
                if (isNaN(parseInt(item))) throw 'Not a number';
            });
            birthday = new Date(birthday[2], parseInt(birthday[1]) - 1, birthday[0]);
            return parseInt((currentDate - birthday) / 31536000000);
        },
        getPassword() {
            return (this.firstName[0]).toUpperCase() + (this.lastName).toLowerCase() + birthday.getFullYear();
        },
    };
    return newUser;
}

let result = createNewUser();
console.log(result.getAge());
console.log(result.getPassword());*/


function strToDate(strDate) {
    const parts = strDate.split('.');
    parts.forEach((item) => {
        if (isNaN(parseInt(item))) throw 'Not a number';
    });
    return new Date(parts[2], parseInt(parts[1]) - 1, parts[0]);
}
function getAge(birthday) {
    const currentDate = new Date();
    return parseInt((currentDate - strToDate(birthday)) / 31536000000);
}
function getPassword(firstName, lastName, birthday) {
    return (firstName[0]).toUpperCase() + (lastName).toLowerCase() + strToDate(birthday).getFullYear();
}

function createNewUser () {
    let firstName = prompt('Enter your name, please');
    let lastName = prompt('Enter your surname');
    let birthday = prompt('Введите дату рождения (в формате dd.mm.yyyy)');

    const newUser = {
        getAge: getAge(birthday),
        getPassword: getPassword(firstName, lastName, birthday),
    };
    console.log(`${newUser.getAge} ${newUser.getPassword}`);
    return newUser;
}


const result = createNewUser();
console.log(result);