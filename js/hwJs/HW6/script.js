function filterBy (arr, type) {
    const newArr = [];
    arr.forEach(function (element) {
        if (typeof element !== type) {
            newArr.push(element)
        }
    })
    return newArr;
};

let res = filterBy([1,2,3, 'wer', [], {}, new Date(), 'dfgdfg', true], 'number');
console.log(res);